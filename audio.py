import discord
from discord.ext import commands
import youtube_dl
import asyncio
import urllib.request
import urllib.parse
from bs4 import BeautifulSoup as bs

discord.opus.load_opus('libopus.so')
youtube_dl.utils.bug_reports_message = lambda: ''

queue=[]

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'before_options': '-nostdin',
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

class Audio:
    def __init__(self,bot):
        self.bot=bot
    
    def plcheck(self, ctx):
        if queue:
            if ctx.voice_client.is_playing():
                pass
            else:
                Audio.play(self, ctx, url=queue[0])
                queue.pop(0)
        else:
            ctx.voice_client.disconnect()

    async def playafter(self, ctx):
        fut = asyncio.run_coroutine_threadsafe(plcheck(), self.bot.loop)
        try:
            fut.result()
        except:
            await ctx.send("An error happened while processing the queue")



    @commands.command(pass_context=True, no_pm=True)
    async def summon(self, ctx, channel=None):
        if ctx.message.author.voice.channel != None:
            channel=ctx.message.author.voice.channel
            if ctx.voice_client is not None:
                return await ctx.voice_client.move_to(channel)
            else:
                await channel.connect()
        else:
            await ctx.send("You are not in a voice channel")

    @commands.command()
    async def leave(self, ctx):
        if ctx.voice_client is None:
            await ctx.send("I am not in a channel right now.")
        else:
            await ctx.voice_client.disconnect()
            
    @commands.command()
    async def skip(self, ctx):
        if ctx.voice_client.is_playing():
            ctx.voice_client.stop()
            await ctx.send("Skipped the current song.")
        else:
            ctx.send("Nothing is playing right now.")

    @commands.command()
    async def play(self, ctx, *, url):

        async def plcheck(ctx):
            if queue:
                player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
                ctx.voice_client.play(player)
                queue.pop(0)
            else:
                ctx.voice_client.disconnect()

        if url == None:
            await ctx.send("Give me a link god dammit")
            return

        elif url[:7]!="http://" or url[:8]!="https://":
            query = urllib.parse.quote(url)
            url = "https://www.youtube.com/results?search_query=" + query
            response = urllib.request.urlopen(url)
            html = response.read()
            soup = bs(html)
            vid=soup.find(attrs={'class':'yt-uix-tile-link'})
            url= 'https://www.youtube.com' + vid['href']


        if ctx.voice_client is None:
            await ctx.message.author.voice.channel.connect()

        if not ctx.voice_client.is_playing():
            async with ctx.typing():
                player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
                ctx.voice_client.play(player, after=playafter)

            await ctx.send('Now playing: **{}**'.format(player.title))
        else:
            queue.append(url)
            await ctx.send("Added the link to the queue")

def setup(bot):
    bot.add_cog(Audio(bot))
