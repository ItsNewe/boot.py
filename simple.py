import discord
import requests
import tweepy
import random
import psutil
import emoji
import string
import re
from dbinteraction import *
from discord.ext import commands

tck=dbexec("SELECT twConsKey FROM bottk")
tcs=dbexec("SELECT twConsSec FROM bottk")
tkk=dbexec("SELECT twTokKey FROM bottk")
tks=dbexec("SELECT twTokSec FROM bottk")
auth= tweepy.OAuthHandler(tck,tcs)
auth.set_access_token(tkk,tks)
tapi= tweepy.API(auth)

class Simple:
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def sys(self, ctx):
        em = discord.Embed(title="System stats", type="rich", colour=discord.Colour(16711680))
        em.add_field(name="CPU", value="{0}%".format(psutil.cpu_percent()), inline=False)
        em.add_field(name="RAM :ram:", value="{0}%".format(psutil.virtual_memory()[2]), inline=False)
        #em.set_thumbnail(self.bot.avatar)
        await ctx.send(embed=em)

    @commands.command()
    async def ping(self, ctx):
        await ctx.send("ws: `"+str(round(ctx.bot.latency*1000,3))+"ms`")

    @commands.command()
    async def neko(self, ctx):
        response = requests.get("http://nekos.life/api/neko")
        print("Response: {}".format(response.status_code))
        wrap = discord.Embed(title="", type="rich")
        wrap.set_image(url=response.json()["neko"])
        await ctx.send(embed=wrap)
    
    @commands.command()
    async def cat(self, ctx, g=None):
        r=""
        if(g=="g"):
            r="/gif"
        response = requests.get("http://cataas.com/cat{}".format(g), verify=False)
        print("Response: {}".format(response.status_code))
        wrap = discord.Embed(title="", type="rich")
        wrap.set_image(url="https://cataas.com/cat")
        await ctx.send(embed=wrap)

    @commands.command()
    async def dog(self, ctx, breed=None, subbreed=None):
        try:
            if not breed:
                r= requests.get("https://dog.ceo/api/breeds/image/random")
            else:
                if(subbreed):
                    r = requests.get("https://dog.ceo/api/breed/{0}/{1}/images/random".format(breed, subbreed))
                else:
                    r = requests.get("https://dog.ceo/api/breed/{0}/images/random".format(breed))
            em = discord.Embed(title=""
            )
            em.set_image(url=r.json()["message"])
            await ctx.send(embed = em)
        except:
            await ctx.send("Bad args\nIs the breed you're looking for a sub-breed?\n**Format:**`$dog [breed] [sub-breed]`\nBreeds list: https://dog.ceo/api/breeds/list/all")

            ###NOT WORKING, MY DUMBASS NEEDS TO FIND OUT HOW IT WORKS###
    @commands.command()
    async def twitteruser(self, ctx,a):
        if(a==None):
            await ctx.send("You need to provide a user.")
            return
        try:
            urlem="https://twitter.com/{}".format(a)
            user = tapi.get_user(a)

            wrap=discord.Embed(Title="Twitter user", type="rich", url=urlem, colour=discord.Colour.from_rgb(0,172,237))
            wrap.set_thumbnail(url=user.profile_image_url)
            wrap.add_field(name="Name", value=user.name)
            wrap.add_field(name="Tweets", value=user.statuses_count)
            wrap.add_field(name="Username", value="@{}".format(user.screen_name), inline=False)
            wrap.add_field(name="Followers", value=user.followers_count)
            wrap.add_field(name="Following", value=user.friends_count)
            wrap.add_field(name="Bio", value=user.description, inline=False)
            await ctx.send(embed=wrap)
        except(tweepy.TweepError):
            await ctx.send("User `{}` not found.".format(a))
            return
        except(Exception):
            await ctx.send("An error occured, please contact **Newe#6184** with the following message```py\n{}```".format(Exception))
            return

    @commands.command()
    async def owo(self, ctx, *, text: commands.clean_content):
        replacement_table = {
            r'[rl]': 'w',
            r'[RL]': 'W',
            r'n([aeiou])': 'ny\\1',
            r'N([aeiou])': 'Ny\\1',
            r'ove': 'uv'
        }
        kao=["OwO", "UwU", ":3"]
        r= random.randint(0,len(kao)-1)
        for regex, replace_with in replacement_table.items():
            text = re.sub(regex, replace_with, text)

        await ctx.send(text+ " "+kao[r])
    @commands.command()
    async def account(self, ctx):
        yesno=dbexec("SELECT * FROM users WHERE id={}".format(ctx.message.author.id))
        if yesno==None:
            v=(ctx.message.author.id, ctx.message.author.name)
            a=dbexec("insert into users values (?,?,NULL)",v, f=False)
            if a=="err":
                ctx.send("error")
            await ctx.send("Account created successfully <:NWKonataThumbsUp:445210207597887489>")
            return
        else:
            await ctx.send("Account **{}** already exists".format(ctx.message.author))

    @commands.command()
    async def invite(self, ctx):
        await ctx.send("https://discordapp.com/api/oauth2/authorize?client_id=366632492590956544&permissions=8&scope=bot")
        
    
def setup(bot):
    bot.add_cog(Simple(bot))
