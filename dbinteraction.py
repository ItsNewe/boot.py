import sqlite3
from sqlite3 import Error
import pprint

def dbexec(query, arg=None, f=True):
    try:
        conn = sqlite3.connect('stor.db')
        c = conn.cursor()
        if arg:
            c.execute(query, arg)
        else:
            c.execute(query)
        
        if f:
            az = c.fetchone()
            conn.commit()
            conn.close()
            if az!=None and az[0]:
                return(az[0])
            else:
                return None

        conn.commit()
        conn.close()

    except Error as e:
        print("\033[91mUne erreur est survenue pendant la gestion de la requête SQL: {}\033[0m".format(e))
        return "err"
