#-*- coding: utf-8 -*-
import discord
import logging
import sys, traceback
import random
import dbinteraction
import api
from discord.ext import commands
import asyncio

bot = commands.Bot(command_prefix='n$')

logging.basicConfig(level=logging.INFO, \
    format='[%(asctime)s] %(levelname)s - [%(name)s] %(message)s')
log = logging.getLogger(__name__)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

plsts={
    "brancher le GPIO":"0",
    "la chaine de castello":"3",
    "Jul":"2",
    "la BO de Taxi 5":"2",
    "NFS Underground 2":"0",
    "Fortnite with Drake":"1",
    "Vsauce":"3"
}

async def autogmchange():
    await asyncio.sleep(300)
    ard=random.randint(1,8)
    stt =dbinteraction.dbexec("SELECT status FROM playstat WHERE ID = {}".format(ard))
    typ= dbinteraction.dbexec("SELECT type FROM playstat WHERE status = '{}'".format(stt))
    await bot.change_presence(activity=discord.Activity(type=typ, name=stt, url='https://twitch.tv/newe1337', details="skrrt skrrt"))

@bot.event
async def on_ready():
    guilds=[]
    ard=random.randint(1,8)
    print("Logged in as {}, d.py V{}".format(bot.user, discord.__version__))
    for g in bot.guilds:
        guilds.append(g.name)
    print("-----------------------------------------------------\n\
    Connected to: {0}\n-----------------------------------------------------".format("\n".join(guilds)))
    stt =dbinteraction.dbexec("SELECT status FROM playstat WHERE ID = {}".format(ard))
    typ= dbinteraction.dbexec("SELECT type FROM playstat WHERE ID = {}".format(ard))
    await bot.change_presence(activity=discord.Activity(type=typ, name=stt, url='https://twitch.tv/newe1337', details="skrrt skrrt"))
    bt=bot.loop.create_task(autogmchange())

@bot.event
async def on_message(message):
    await bot.process_commands(message)

@bot.event
async def on_command(ctx):
    print("{} [COMMAND] | {} : {}{}".format(bcolors.WARNING, ctx.message.author, ctx.message.clean_content[2:], bcolors.ENDC))

@bot.event
async def on_command_error(ctx, exception):
    if str(exception)[12:]=="is not found":
        return
    if ctx.message.author.id==131135803232354305:
        await ctx.send("Erreur lors du traitement de la commande\n```\n{}```".format(exception))
    else:
        print(exception)

@bot.event
async def on_guild_join(guild):
    try:
        em = discord.Embed(title="Hello ໒( ●ᴥ ●)ʋ", color=discord.Color(0xff0000))
        em.set_thumbnail(url=discord.ClientUser.avatar_url)
        em.add_field(name="test", value="Hello, i'm Boot, a capable and experimental discord bot!\nTo see the list of my commands, type **__n$__help**\nPlease not that i may go down unexpectedly due to the dev tesing things, so in the case this happens, i'll be back up and stable in around 30 minutes!")
        ch= guild.system_channel
        if not ch:
            for c in guild.text_channels:
                if not c.permissions_for(guild.me).send_messages:
                    continue
                ch= c
        ch.send("this is a test")

    except(Exception) as e:
        print(e)

@bot.command()
@commands.is_owner()
async def load(ctx, extension_name : str):
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await ctx.send(":white_check_mark: Loaded {} ".format(extension_name))

@bot.command()
@commands.is_owner()
async def unload(ctx, extension_name : str):
    try:
        bot.unload_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await ctx.send(":white_check_mark: Unloaded {}".format(extension_name))

@bot.command()
@commands.is_owner()
async def reload(ctx, extension_name : str):
    try:
        bot.unload_extension(extension_name)
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await ctx.send(":white_check_mark: Reloaded `{}` module".format(extension_name))

initial_extensions = ['simple', "owner", "admin", "osu", "audio"]
if __name__ == "__main__":
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension `{}\n{}`'.format(extension, exc))
            
print('{}Successfully logged in and booted...!{}'.format(bcolors.OKGREEN, bcolors.ENDC))

bot.run(dbinteraction.dbexec("SELECT dstk FROM bottk"), reconnect=True)
