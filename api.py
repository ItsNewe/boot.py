from flask import Flask, request
app = Flask(__name__)

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

def run1():
    app.run(host='0.0.0.0',port=6969, debug=False, use_reloader=False)
    print("API IS ONLINE ON PORT 6969")

@app.route('/')
def ohayo():
    return("おはよう　('ω')ノ")

@app.route('/shutdown', methods=['GET'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'



if __name__ == "__main__":
    run1()