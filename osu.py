import discord
import requests
import dbinteraction
from discord.ext import commands

class Osu:
    def __init__(self,bot):
        self.bot = bot
    k = dbinteraction.dbexec("SELECT osukey FROM bottk")

    @commands.command()
    async def osu(self, ctx, user=None, raw=False):
        if user==None:
            try:
                a=dbinteraction.dbexec("SELECT osuname FROM users WHERE id = {}".format(ctx.message.author.id))
                if a!=None:
                    user=a
            except:
                await ctx.send("An error occured")
        try:
            response = requests.get("https://osu.ppy.sh/api/get_user?k={0}&u={1}&mode=0&type=string".format(self.k, user))
            rj = response.json()[0]
            if(raw):
                await ctx.send("**RAW JSON DATA**\n```json\n{0}```".format(rj))
            else:
                em = discord.Embed(title="Osu profile for {0} *[ID:{1}]*".format(rj["username"], rj["user_id"]), color=discord.Color(0xFD00FD))
                em.set_thumbnail(url="https://a.ppy.sh/{}".format(rj["user_id"]))
                em.add_field(name="Level", value=round(float(rj["level"]), 0))
                em.add_field(name="Country", value=rj["country"])
                em.add_field(name="PP", value=round(float(rj["pp_raw"]), 0))
                em.add_field(name="Rank", value="#{}".format(rj["pp_rank"]))
                em.add_field(name="Accuracy", value="{}%".format(round(float(rj["accuracy"]), 2)))
                em.add_field(name="Play count", value=rj["playcount"])
                em.add_field(name="SS ranks", value=rj["count_rank_ss"])
                em.add_field(name="S ranks", value=rj["count_rank_s"])
                em.add_field(name="A ranks", value=rj["count_rank_a"])
                em.set_footer(text="Requested by {}".format(ctx.message.author))
                await ctx.send(embed=em)
        except:
            await ctx.send("The provided user could not be found.")
    
    @commands.command()
    async def osulink(self, ctx, user=""):
        try:
            a=dbinteraction.dbexec("SELECT osuname WHERE id = {}".format(ctx.message.author.id))
            if a!=None:
                await ctx.send("Already in database")
            else:
                t = (user,)
                ch= ctx.message.author
                dbinteraction.dbexec("INSERT INTO users values({},{},{})".format(ctx.message.author.id, ch-ch[3:], t), f=False)
        except:
            print("err")

def setup(bot):
    bot.add_cog(Osu(bot))

    