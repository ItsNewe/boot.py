import discord
import random
from discord.ext import commands

class Admin:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self,ctx, usr: discord.User, *, reas: str="No reason provided"):
        try:
            if(usr==None):
                await ctx.send("User **{}** doesn't exist")
                return

            await ctx.guild.kick(user=usr, reason=reas)
            
        except(PermissionError):
            await ctx.send("I don't have enough permissions to perform this action.")
            return

        except(discord.Forbidden):
            await ctx.send("Insufficient permissions, only server admins can run this comand.")
            return

        except(Exception) as e:
            await ctx.send("```{}```".format(e))
            return
        await ctx.send("Successfully kicked **{}**".format(usr.name))
    
    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def ban(self,ctx, usr: discord.User, *, reas: str="No reason provided"):
        try:
            if(usr==None):
                await ctx.send("User **{}** doesn't exist")
                return

            await ctx.guild.ban(user=usr, reason="[Boot] "+reas)
            
        except(PermissionError):
            await ctx.send("I don't have enough permissions to perform this action.")
            return

        except(discord.Forbidden):
            await ctx.send("Insufficient permissions, only server admins can run this comand.")
            return

        except(Exception) as e:
            await ctx.send("```{}```".format(e))
            return
        await ctx.send("Successfully banned **{}**".format(usr.name))
    
    @commands.command()
    async def status(self,ctx):
        await ctx.send("```\nUSER: {0}\nWS: {1}\nGUILDS: {2}\nLOOP: {3}\nD.PY VER: {4}```".format(self.bot.user, self.bot.ws, self.bot.guilds, self.bot.loop, discord.__version__))

def setup(bot):
    bot.add_cog(Admin(bot))
