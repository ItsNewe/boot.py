#-*- coding: utf-8 -*-
import discord
import random
from discord.ext import commands
import dbinteraction
class Owner:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner()
    async def changegame(self, ctx):
        try:
            ard=random.randint(1,8)
            stt =dbinteraction.dbexec("SELECT status FROM playstat WHERE ID = {}".format(ard))
            typ= dbinteraction.dbexec("SELECT type FROM playstat WHERE ID = '{}'".format(ard))
            await self.bot.change_presence(activity=discord.Activity(type=typ, name=stt, url='https://twitch.tv/newe1337', details="skrrt skrrt"))
        except(IndexError):
            pass
        await ctx.send(":ok_hand:")
        await ctx.send("Now playing **{0}**".format(self.bot.discord.Game.name))

    @commands.command()
    @commands.is_owner()
    async def logout(self, ctx):
        await ctx.send(":wave:")
        await self.bot.logout()

    ###NOT WORKING FOR NOW###
    @commands.command()
    @commands.is_owner()
    async def restart(self, ctx):
        await ctx.send(":wave:")
        await self.bot.clear()


def setup(bot):
    bot.add_cog(Owner(bot))